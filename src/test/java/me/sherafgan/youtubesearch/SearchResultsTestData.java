package me.sherafgan.youtubesearch;

import com.google.api.services.youtube.model.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeAll;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsTestData {
    public static final String TEST_ITEM_ID = "xcJtL7QggTI";
    public static final String TEST_ITEM_URL = "https:\\/\\/i.ytimg.com\\/vi\\/xcJtL7QggTI\\/default.jpg";
    public static final String TEST_ITEM_KIND = "youtube#video";
    public static final String TEST_ITEM_TITLE = "Sony 4K Demo: Another World!";

    protected static List<SearchResult> searchResults;

    @BeforeAll
    static void init() {
        ResourceId resourceId = new ResourceId();
        resourceId.setVideoId(TEST_ITEM_ID);
        resourceId.setKind(TEST_ITEM_KIND);
        SearchResult searchResult = new SearchResult();
        searchResult.setId(resourceId);
        SearchResultSnippet snippet = new SearchResultSnippet();
        snippet.setThumbnails(new ThumbnailDetails().setDefault(new Thumbnail().setUrl(TEST_ITEM_URL)));
        snippet.setTitle(TEST_ITEM_TITLE);
        searchResult.setSnippet(snippet);
        searchResults = ImmutableList.of(searchResult);
    }
}
