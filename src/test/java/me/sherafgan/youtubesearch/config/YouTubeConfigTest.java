package me.sherafgan.youtubesearch.config;

import com.google.api.services.youtube.YouTube;
import me.sherafgan.youtubesearch.domain.YouTubeApiConstants;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class YouTubeConfigTest {
    @Autowired
    YouTube.Search.List youTubeSearch;

    @Test
    void checkYouTubeSearchReturnedWithProperties() throws IOException {
        assertNotNull(youTubeSearch);
        YouTube youTube = youTubeSearch.getAbstractGoogleClient();
        assertEquals(youTube.getApplicationName(), YouTubeApiConstants.YOUTUBE_APPLICATION_NAME);
        assertThat(youTube.getJsonFactory(), instanceOf(YouTubeApiConstants.JSON_FACTORY.getClass()));
        assertThat(youTube.getRequestFactory().getTransport(), instanceOf(YouTubeApiConstants.HTTP_TRANSPORT.getClass()));
    }
}