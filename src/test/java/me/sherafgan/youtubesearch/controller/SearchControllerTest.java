package me.sherafgan.youtubesearch.controller;

import me.sherafgan.youtubesearch.SearchResultsTestData;
import me.sherafgan.youtubesearch.exceptions.YouTubeServiceUnavailableException;
import me.sherafgan.youtubesearch.service.YouTubeApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SearchController.class)
class SearchControllerTest extends SearchResultsTestData {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    YouTubeApiService youTubeApiService;

    @BeforeEach
    void setUp() throws YouTubeServiceUnavailableException {
        when(youTubeApiService.searchYouTube("demo")).thenReturn(super.searchResults);
    }

    @Test
    void getSearchResults() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/search").param("searchQuery", "demo")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id.kind").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id.videoId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].snippet.thumbnails.default.url").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].snippet.title").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id.kind").isNotEmpty());
    }
}