package me.sherafgan.youtubesearch.service;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import me.sherafgan.youtubesearch.SearchResultsTestData;
import me.sherafgan.youtubesearch.exceptions.YouTubeServiceUnavailableException;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class YouTubeApiServiceTest extends SearchResultsTestData {
    @Autowired
    private YouTubeApiService youTubeApiService;

    @Autowired
    @MockBean
    private YouTube.Search.List youTubeSearch;

    @Test
    void searchYouTubeTestSuccessful() throws IOException {
        SearchListResponse searchListResponse = new SearchListResponse().setItems(searchResults);
        when(youTubeSearch.execute()).thenReturn(searchListResponse);

        List<SearchResult> searchResults = youTubeApiService.searchYouTube("demo");
        assertNotNull(searchResults);
        assertEquals(1, searchResults.size());
        JSONObject jsonObject = new JSONObject(searchResults.get(0));
        Object resultsDocument = Configuration.defaultConfiguration().jsonProvider().parse(jsonObject.toJSONString());

        String resultItemKind = JsonPath.read(resultsDocument, "$.id.kind");
        assertEquals(SearchResultsTestData.TEST_ITEM_KIND, resultItemKind);

        String resultItemVideoId = JsonPath.read(resultsDocument, "$.id.videoId");
        assertEquals(SearchResultsTestData.TEST_ITEM_ID, resultItemVideoId);

        String resultItemThumbnailUrl = JsonPath.read(resultsDocument, "$.snippet.thumbnails.default.url");
        assertEquals(SearchResultsTestData.TEST_ITEM_URL, resultItemThumbnailUrl);

        String resultItemThumbnailTitle = JsonPath.read(resultsDocument, "$.snippet.title");
        assertEquals(SearchResultsTestData.TEST_ITEM_TITLE, resultItemThumbnailTitle);
    }

    @Test
    void searchYouTubeTestShouldThrowException() throws IOException {
        when(youTubeSearch.execute()).thenThrow(new IOException("searchYouTubeTestShouldThrowException"));

        assertThrows(YouTubeServiceUnavailableException.class, () -> {
            youTubeApiService.searchYouTube("demo");
        });

//        assertDoesNotThrow(() -> {
//            youTubeApiService.searchYouTube("demo");
//        }, YouTubeSearchException.EXCEPTION_MESSAGE);
    }

    @Test
    void searchYouTubeReturnsZeroResults() throws IOException {
        SearchListResponse searchListResponse = new SearchListResponse();
        when(youTubeSearch.execute()).thenReturn(searchListResponse);

        List<SearchResult> searchResults = youTubeApiService.searchYouTube("demo");
        assertNotNull(searchResults);
        assertEquals(0, searchResults.size());
    }
}