package me.sherafgan.youtubesearch.config;

import com.google.api.services.youtube.YouTube;
import me.sherafgan.youtubesearch.domain.YouTubeApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.IOException;

@Configuration
public class YouTubeConfig {
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public YouTube getYoutube() {
        return new YouTube.Builder(YouTubeApiConstants.HTTP_TRANSPORT, YouTubeApiConstants.JSON_FACTORY, request -> {
        }).setApplicationName(YouTubeApiConstants.YOUTUBE_APPLICATION_NAME).build();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public YouTube.Search.List getYouTubeSearch(@Autowired YouTube youTube) throws IOException {
        return youTube.search().list("id,snippet");
    }
}
