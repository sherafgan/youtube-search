package me.sherafgan.youtubesearch.exceptions;

import me.sherafgan.youtubesearch.domain.YouTubeApiConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = YouTubeApiConstants.EXCEPTION_MESSAGE)
public class YouTubeServiceUnavailableException extends RuntimeException {

    public YouTubeServiceUnavailableException(String message, Throwable e) {
        super(message, e);
    }
}
