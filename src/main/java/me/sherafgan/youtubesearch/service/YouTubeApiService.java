package me.sherafgan.youtubesearch.service;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import me.sherafgan.youtubesearch.domain.YouTubeApiConstants;
import me.sherafgan.youtubesearch.exceptions.YouTubeServiceUnavailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
public class YouTubeApiService {
    /**
     * Define a logger that logs the search execution to YouTube Data API.
     */
    private static final Logger logger = LoggerFactory.getLogger(YouTubeApiService.class);

    /**
     * {@link com.google.api.services.youtube.YouTube.Search.List} instance
     * necessary for search execution against YouTube Data API.
     */
    private final YouTube.Search.List youTubeSearch;

    /**
     * YouTube API developer key retrieved from Google Dev Console.
     */
    private final String apiKey;

    public YouTubeApiService(YouTube.Search.List youTubeSearch, @Value("${youtube.apikey}") String apikey) {
        this.youTubeSearch = youTubeSearch;
        this.apiKey = apikey;
    }

    /**
     * Runs {@code searchQuery} against the YouTube API.
     *
     * @param searchQuery - search query text.
     * @return {@link List} of {@link SearchResult}s if successful.
     * @throws YouTubeServiceUnavailableException if the YouTube API throws IOException.
     */
    public List<SearchResult> searchYouTube(String searchQuery) throws YouTubeServiceUnavailableException {
        List<SearchResult> searchResults = executeSearch(searchQuery);
        logger.debug("YouTube API returned " + searchResults.size() + " videos.");
        return searchResults;
    }

    private List<SearchResult> executeSearch(String searchQuery) throws YouTubeServiceUnavailableException {
        youTubeSearch.setKey(apiKey);
        youTubeSearch.setQ(searchQuery);
        youTubeSearch.setType("video");
        youTubeSearch.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
        youTubeSearch.setMaxResults(YouTubeApiConstants.NUMBER_OF_VIDEOS_RETURNED);
        try {
            SearchListResponse searchResponse = youTubeSearch.execute();
            List<SearchResult> responseItems = searchResponse.getItems();
            if (responseItems != null && !responseItems.isEmpty()) {
                return responseItems;
            }
        } catch (IOException e) {
            throw new YouTubeServiceUnavailableException(YouTubeApiConstants.EXCEPTION_MESSAGE, e);
        }
        return Collections.emptyList();
    }
}
