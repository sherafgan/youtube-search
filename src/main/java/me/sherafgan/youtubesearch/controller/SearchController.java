package me.sherafgan.youtubesearch.controller;

import com.google.api.services.youtube.model.SearchResult;
import me.sherafgan.youtubesearch.exceptions.YouTubeServiceUnavailableException;
import me.sherafgan.youtubesearch.service.YouTubeApiService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SearchController {
    private final YouTubeApiService youTubeApiService;

    public SearchController(YouTubeApiService youTubeApiService) {
        this.youTubeApiService = youTubeApiService;
    }

    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SearchResult> getSearchResults(@RequestParam String searchQuery) throws YouTubeServiceUnavailableException {
        return youTubeApiService.searchYouTube(searchQuery);
    }
}
