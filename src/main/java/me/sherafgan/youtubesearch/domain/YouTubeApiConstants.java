package me.sherafgan.youtubesearch.domain;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

public class YouTubeApiConstants {

    /**
     * Define a global instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    /**
     * Define a global instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new JacksonFactory();
    /**
     * Define a global variable that determines the number of
     * videos returned from the YouTube Data API.
     */
    public static final long NUMBER_OF_VIDEOS_RETURNED = 50;
    /**
     * Define YouTube object application name.
     */
    public static final String YOUTUBE_APPLICATION_NAME = "youtube-search-demo";
    /**
     * Define custom exception thrown if YouTube API is not available.
     */
    public static final String EXCEPTION_MESSAGE = "YouTube API failed (IOException) during search execution!";

    private YouTubeApiConstants() {
    }
}
